Source: hhinnant-date
Section: libdevel
Priority: optional
Maintainer: Martin Tammvee <martin.tammvee@cleveron.com>
Homepage: https://github.com/HowardHinnant/date
Vcs-Git: https://gitlab.com/cleveron/debian/date.git
Vcs-Browser: https://gitlab.com/cleveron/debian/date
Rules-Requires-Root: no
Standards-Version: 4.6.0
Build-Depends: cmake, debhelper-compat (= 13), ninja-build

Package: libdate-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libdate-tz3 (= ${binary:Version}), ${misc:Depends}
Description: Date and time library based on the <chrono> header (development files)
 "date.h" is a header-only library which builds upon <chrono>. It adds some new
 duration types, and new time_point types. It also adds "field" types such as
 year_month_day which is a struct {year, month, day}. And it provides
 convenient means to convert between the "field" types and the time_point types.

Package: libdate-tz3
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Date and time library based on the <chrono> header
 "tz.h" are a timezone library built on top of the "date.h" library. This
 timezone library is a complete parser of the IANA timezone database. It
 provides for an easy way to access all of the data in this database, using the
 types from "date.h" and <chrono>. The IANA database also includes data on leap
 seconds, and this library provides utilities to compute with that information
 as well.
